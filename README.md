# SELF-SERVICE WEBSITE USING REACT

## SETUP

1. clone this repository (master branch) with running `git clone https://thariq_w@bitbucket.org/513-solution/voxnet-customer-reg-react.git` on your local terminal
2. download all dependencies by `npm install` at root directory of your project.
   It will take a while
3. start the server with `npm start`
4. wait for a while. the app will automatically launch on your default browser
5. pull other branch if needed

if you have a problem or question, please contact me :)
Regards,
Thariq Warsahemas
