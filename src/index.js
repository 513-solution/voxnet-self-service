import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import { UserContextProvider, ProductContextProvider } from "context";

ReactDOM.render(
  <React.StrictMode>
    <ProductContextProvider>
      <UserContextProvider>
        <App />
      </UserContextProvider>
    </ProductContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
