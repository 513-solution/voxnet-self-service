import { getURL } from "./base";

// FETCH CITY'S TELEPHONE NUMBER OPTION
export async function fetchCityTel() {
  const cities = await getURL("/Product/CityServed")
    .then((res) => {
      const allDatas = res.data.data;
      let parsedData = {};
      allDatas.forEach(({ pcs_city }) => {
        parsedData[pcs_city] = { value: pcs_city, label: pcs_city };
      });
      return parsedData;
    })
    .catch((err) => false);
  return cities;
}
