# ON GOING

- phone-input invalid-feedback is not displayed

- cannot login

- return co_id for extend and add addon

- trunkline vs concurrent call -> kasusnya begini
  1 customer dgn jumlah sub accounts (users atau extensions) antar cabang banyak, istilah trunk lines akan lebih tetap diartikan sebagai concurrent calls (jalur bicara bersamaan). 2 customer yg benar2x menerima jumlah panggilan dari luar atau menelepon keluar, istilah trunk lines lebih pas. jadi sebaiknya diberi tanda kurung utk istilah yg sama (concurrent call)

# CLEARED!!!

- only some data might inserted to table, bcs Multiple API to insert data to tables in series (solution: use single api to insert to all customer and order tables)

- register - emailsmtp's invoice addon sub total is 0

- /CUSTOMERADDRESS RETURN 500 UNLESS YOU FILLED SOME NUMBERS OF ITS FIELD. DONT KNOW WHICH!

- update pdn_state if taken

- ARRAY OF ALL USER FROM CUSTOMER_COMPANIES IS STORED IN FRONTEND

- COT_TELEPHONE DEFAULT TO 1, NOT 0

- ORDERTABLE: setara dengan 120 hari mungkin bisa diganti -> Masa berlangganan 120 hari

- REORDER ACCOUNT FORM FIELDS!

- PhoneField validation, unique validation, style

- NOTE STAR ON ORDER TABLE DOESN'T MATCH

- CHOOSE DID NUMBERS (HAVEN'T ASSIGNED TO UC OR FETCH FROM ACTUAL TABLES' DATA)

- Client-side authentification using Cookies, so not logged out everytime refresh the pages

- unique cu_name_mobile not working (reason: dial_code is undefined)
