# DE-CATEGORIZED FOR SELF-REGISTRATION FORM

## Account Info

```
{
  cc_name: REQUIRED UNIQUE
  cu_name_dsp: REQUIRED
  cu_name_email: REQUIRED UNIQUE
  cu_name_mobile: REQUIRED UNIQUE
}
```

## Billing Info

```
{
  cu_name_first: REQUIRED
  cu_name_last

  ca_attention: REQUIRED
  ca_address_1
  ca_lr_id : FETCH
  ca_lp_id : FETCH
  ca_zipcode
  ca_country
  cc_phone
  ca_fax
  cc_web
}
``

```
