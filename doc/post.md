# /CustomerUser/new

cu_id
cc_id
cu_name_first: REQUIRED
cu_name_last
cu_name_email: REQUIRED UNIQUE
cu_name_mobile: REQUIRED UNIQUE

# /CustomerAddress/new

ca_id
cc_id
ca_address_2
ca_attention: REQUIRED
ca_address_1
ca_lr_id
ca_lp_id
ca_zipcode
ca_fax
ca_country

# /CustomerCompanies/new

cc_id
cc_name: REQUIRED UNIQUE
cc_phone
cc_web

# /CustomerOrder/new

p_id
// cc_id
// co_num_ip
// co_num_inv
// co_num_transdate
// co_num_transcode
//co_num_transidmerchant
co_name
co_name_telnumber
co_name_trunknum
co_name_talkcredit
co_name_talkdur
co_name_maxsub
co_name_maxtalkcredit
co_name_avgsubscribe
co_name_monthlysubscribe
co_status
co_createdate
co_activedate

# /CustomerOrderAddon/new

coa_id
co_id
pa_id
pam_id
coa_value
coa_price
coa_qty
coa_price_total

/CustomerUser/new
/CustomerAddress/new
/CustomerCompanies/new

/CustomerOrder/new
/CustomerOrderAddon/new
