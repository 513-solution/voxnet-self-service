# FROM SALES ADMIN'S DATABASE

## customer_address_category

```
{
  // cac_id   : 1, 2
  // cac_name : Alamat kantor(default), Alamat penagihan
}
```

## customer_address

```
{
  // ca_id
  // cc_id
  // ca_address_2
  ca_attention: REQUIRED
  ca_address_1
  ca_lr_id
  ca_lp_id
  ca_zipcode
  ca_fax
  ca_country
}
```

## customer_companies

```
{
  // cc_id
  cc_name: REQUIRED UNIQUE
  cc_phone
  cc_web
}
```

## customer_user

```
{
  // cu_id
  // cc_id
  cu_name_first: REQUIRED
  cu_name_last
  cu_name_dsp: REQUIRED
  cu_name_email: REQUIRED UNIQUE
  cu_name_mobile: REQUIRED UNIQUE
}
```
